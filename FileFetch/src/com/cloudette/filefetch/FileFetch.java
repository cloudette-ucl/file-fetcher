package com.cloudette.filefetch;

import java.awt.Desktop;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

public class FileFetch {

	private static String skyDrivePath;
	private static final String FETCH = "FILE FETCH";
	private static final String FOLDER_NAME = "SkyDrive\\";
	private static final String CLOUDETTE_FOLDER = "/media/CLOUDETTE/";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("File system: " + args[0]);
		try {
			sendQuery(args[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// function not done yet
		private static void sendQuery(String filename) throws IOException, InterruptedException, ClassNotFoundException
		{
			Socket sock = new Socket("192.168.42.1", 6789);
			sock.setKeepAlive(true);

			DataOutputStream toServ = new DataOutputStream(sock.getOutputStream());

			int index = filename.indexOf(FOLDER_NAME, 0);
			StringBuilder sb = new StringBuilder();
			sb.append(CLOUDETTE_FOLDER);
			for(int i = index;i<filename.length();i++)
				sb.append(filename.charAt(i));
			toServ.writeBytes(FETCH + " " + sb.toString() + "\n");
			toServ.flush();

			Thread.sleep(8000);

			DataInputStream dis = new DataInputStream(sock.getInputStream());
			byte[] buffer = new byte[1024];

			FileOutputStream fos = new FileOutputStream(filename);

			while(dis.read(buffer) != -1) {
				fos.write(buffer);
			}
			
			System.out.println("File received.");
			sock.close();
			dis.close();
			fos.close();
			
			File file = new File(filename);
			
			openFile(file);
		}
	
	private static void openFile(File file) throws IOException
	{
		Desktop.getDesktop().open(file);
	}
}
